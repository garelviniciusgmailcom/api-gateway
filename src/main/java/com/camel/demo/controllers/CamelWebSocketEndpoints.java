package com.camel.demo.controllers;

import org.apache.camel.builder.RouteBuilder;

public class CamelWebSocketEndpoints extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("kafka:customers?brokers={{kafka-host}}:{{kafka-port}}")
                .routeId("fromJMStoWebSocket")
                .to("websocket://localhost:8443/newsTopic?sendToAll=true&staticResources=classpath:webapp");

        from("direct:websocket-service")
                .log("${body}")
                .routeId("fromJMStoWebSocket")
                .to("websocket://localhost:8443/newsTopic?sendToAll=true&staticResources=classpath:webapp");
    }
}
