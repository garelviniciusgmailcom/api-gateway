package com.camel.demo.controllers;

import java.util.LinkedList;
import java.util.List;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.util.toolbox.AggregationStrategies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CamelRestEndpoints extends RouteBuilder {

    @Value("${service.host}")
    private String serviceHost;

    @Override
    public void configure() throws Exception {

        /*
         * Common rest configuration
         */
        restConfiguration()
                .host(serviceHost)
                .bindingMode(RestBindingMode.json)
                .contextPath("/api")
                .apiContextPath("/doc")
                    .apiProperty("api.title", "Patterns Camel ")
                    .apiProperty("api.description", "Operations that can be invoked in the Camel demo service")
                    .apiProperty("api.license.name", "Apache License Version 2.0")
                    .apiProperty("api.license.url", "http://www.apache.org/licenses/LICENSE-2.0.html")
                    .apiProperty("api.version", "1.0.0");

        /*
         * EIPs
         */
        // full path: /api/patters
        rest("/patters")

                .get("/simple-response")
                    .description("consumer messages from kafka topic")
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:simple-response").endRest()

                .get("/consuming-protected-service")
                    .description("consumer protected messages from service")
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:service-protected").endRest()

                .get("/consuming-from-soap")
                    .description("consumer messages from soap service")
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:soap").endRest()

                .post("/write-file")
                    .description("producer messages to kafka topic")
                    .type(Object.class)
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:write-file").endRest()

                .get("/read-file")
                    .description("consumer messages from soap service")
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:read-file").endRest()

                .post("/producer-topic-kafka")
                    .description("producer messages to kafka topic")
                    .type(Object.class)
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:kafka-producer").endRest()

                .get("/consumer-topic-kafka")
                    .description("consumer messages from kafka topic")
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:kafka-consumer").endRest()

                .post("/rabbitmq-request-reply")
                    .description("request-reply messages from rabbit topic")
                    .type(Object.class)
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .to("direct:rabbitmq-request-reply").endRest()

                .get("/multicast-aggregation")
                    .description("Invoke all microservices in parallel")
                    .outType(Object.class)
                    .apiDocs(true)
                    .responseMessage().code(200).message("OK").endResponseMessage()
                    .route()
                        .multicast(AggregationStrategies.flexible().accumulateInCollection(LinkedList.class))
                        .parallelProcessing()
                            .to("direct:aloha")
                            .to("direct:hola")
                            .to("direct:ola")
                            .to("direct:bonjour")
                        .end()
                        .transform().body(List.class, list -> list)
                        .setHeader("Access-Control-Allow-Credentials", constant("true"))
                        .setHeader("Access-Control-Allow-Origin", header("Origin"));
    }
}
