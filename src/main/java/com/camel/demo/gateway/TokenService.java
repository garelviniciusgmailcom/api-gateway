package com.camel.demo.gateway;

public interface TokenService {

    String findTokenByService(String service);
}
