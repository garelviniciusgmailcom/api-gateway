package com.camel.demo.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class RedisIteractors implements TokenService {

    @Autowired
    private Jedis jedis;

    @Override
    public String findTokenByService(String hash) {
        return jedis.get(hash);
    }
}
