package com.camel.demo.iteractors;

import com.camel.demo.gateway.TokenService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

//@Service
public class AuthorizationHeaderProcessor implements Processor {

    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer";

    private String service;

    @Autowired
    private TokenService tokenService;

    public AuthorizationHeaderProcessor(String service) {
        this.service = service;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, Object> headers = exchange.getIn().getHeaders();
        String token = new StringBuilder(BEARER).append(" ").append(this.tokenService.findTokenByService(service)).toString();
        headers.put(AUTHORIZATION, token);
        exchange.getIn().setHeaders(headers);
    }
}
