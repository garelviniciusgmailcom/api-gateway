package com.camel.demo.routes;

import com.camel.demo.iteractors.AuthorizationHeaderProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

@Component
public class CamelServiceRoutes extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        /*
         * Definition of the external services: simple-response
         */
        from("direct:simple-response")
                .id("simple-response")
                .removeHeaders("accept*")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.ACCEPT_CONTENT_TYPE, constant("application/json"))
                .hystrix()
                    .hystrixConfiguration().executionTimeoutInMilliseconds(1000).circuitBreakerRequestVolumeThreshold(5).end()
                    .id("simple-response")
                    .groupKey("http://simple-response:8080/")
                    .to("http4://demo7124846.mockable.io/simple-response?bridgeEndpoint=true&connectionClose=true")
                    .unmarshal( new JacksonDataFormat() )
                .onFallback()
                    .transform().constant("simple response (fallback)")
                .end();

        /*
         * Definition of the external services: service-protected
         */
        from("direct:service-protected")
                .id("service-protected")
                .removeHeaders("accept*")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .setHeader(Exchange.ACCEPT_CONTENT_TYPE, constant("application/json"))
                .hystrix()
                    .hystrixConfiguration().executionTimeoutInMilliseconds(1000).circuitBreakerRequestVolumeThreshold(5).end()
                    .id("service-protected-id")
                    .groupKey("http://service-protected:8080/")
                    .process(new AuthorizationHeaderProcessor("service-protected"))
                    .to("http4:localhost:8080/api/service-protected?bridgeEndpoint=true&connectionClose=true")
                    .convertBodyTo(String.class)
                .onFallback()
                    .transform().constant("service-protected response (fallback)")
                .end();

        /*
         * Definition of the external services: SOAP
         */
        from("direct:soap")
                .id("soap")
                .setHeader(Exchange.ACCEPT_CONTENT_TYPE, constant("application/xml"))
                .setHeader(CxfConstants.OPERATION_NAME, constant("GetBook"))
                .setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://www.cleverbuilder.com/BookService/"))
                .hystrix()
                    .hystrixConfiguration().executionTimeoutInMilliseconds(1000).circuitBreakerRequestVolumeThreshold(5).end()
                    .id("soap")
                    .groupKey("http://soap:8080/")
                    .to("cxf://http://demo7124846.mockable.io/wsl?dataFormat=RAW"
    //                    + "&serviceClass=com.camel.demo.iteractors"
    //                    + "&wsdlURL=/wsdl/BookService.wsdl"
                    )
                    .convertBodyTo(String.class)
                .onFallback()
                    .transform().constant("soap response (fallback)")
                .end();

        from("direct:write-file")
                .id("write-file")
                .removeHeaders("accept*")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.ACCEPT_CONTENT_TYPE, constant("text/plain"))
                .hystrix()
                    .hystrixConfiguration().executionTimeoutInMilliseconds(1000).circuitBreakerRequestVolumeThreshold(5).end()
                    .id("write-file")
                    .groupKey("http://write-file:8080/")
                    .to("file:src/data?noop=true")
                    .convertBodyTo(String.class)
                .onFallback()
                    .transform().constant("write-file response (fallback)")
                .end();

        from("file:src/data?noop=true")
                .id("read")
                .removeHeaders("accept*")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.ACCEPT_CONTENT_TYPE, constant("text/plain"))
                .hystrix()
                    .hystrixConfiguration().executionTimeoutInMilliseconds(1000).circuitBreakerRequestVolumeThreshold(5).end()
                    .id("read-file")
                    .groupKey("http://read-file:8080/")
                    .to("direct:read-file")
                    .convertBodyTo(String.class)
                .onFallback()
                    .transform().constant("write-file response (fallback)")
                .end();
    }
}
