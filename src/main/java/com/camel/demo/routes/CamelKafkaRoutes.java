package com.camel.demo.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;

import static org.apache.camel.component.kafka.KafkaConstants.TOPIC;

public class CamelKafkaRoutes extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        String topicName = "topic=customers";
        String kafkaServer = "kafka:{{kafka-host}}:{{kafka-port}}";
        String zooKeeperHost = "zookeeperHost={{zookeeper-host}}&zookeeperPort={{zookeeper-port}}";
        String serializerClass = "serializerClass=kafka.serializer.JsonEncoder";

        String toKafka = new StringBuilder().append(kafkaServer).append("?").append(topicName).append("&")
                .append(zooKeeperHost).append("&").append(serializerClass).toString();

        from("file:C:/inbox?noop=true").split().tokenize("\n").to(toKafka);

        // PRODUCER
        from("direct:kafka-producer")
                .log("kafka-producer: ${body}")
                .setHeader(KafkaConstants.KEY, constant("Camel")) // Key of the message
                .to("kafka:customers?brokers={{kafka-host}}:{{kafka-port}}");

        // CONSUMER
        from("kafka:" + TOPIC + "?brokers={{kafka-host}}:{{kafka-port}}" +
                // Setup the topic and broker address
                "&groupId=A" +
                // The consumer processor group ID
                "&autoOffsetReset=earliest" +
                // Ask to start from the beginning if we have unknown offset
                "&offsetRepository=#offsetRepo")
                // Keep the offsets in the previously configured repository
                .to("direct:kafka-consumer");
    }

}
